
public class HW5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		draw18(2);
		draw18(3);
		draw18(4);
		draw18(5);
		
		draw19(2);
		draw19(3);
		draw19(4);
		draw19(5);
		
		draw20(2);
		draw20(3);
		draw20(4);
		draw20(5);
		
		draw21(2);
		draw21(3);
		draw21(4);
		draw21(5);
		
		draw22(2);
		draw22(3);
		draw22(4);
		draw22(5);
		
		
		draw23(2);
		draw23(3);
		draw23(4);
		draw23(5);
		
		draw24(2);
		draw24(3);
		draw24(4);
		draw24(5);
		
		draw25(2);
		draw25(3);
		draw25(4);
		draw25(5);
		
		
	}
	
	public static void draw18(int num){
		
		int round = num;
		for( int i=1; i<=num ; i++ ){
			
			for( int r=1; r<=num ; r++ ){
				
				if(r >= round){
					System.out.print("*");
				}else {
					System.out.print("_");
				}
				
			}

			System.out.println();
			round--;;
		}
		
		System.out.println("");
		
	}
	
	public static void draw19(int num){
		
		int round = 1;
		for( int i=1; i<=num ; i++ ){
			
			for( int r=1; r<=num ; r++ ){
				
				if(r >= round){
					System.out.print("*");
				}else {
					System.out.print("_");
				}
				
			}

			System.out.println();
			round++;
		}
		
		System.out.println("");
		
	}

	public static void draw20(int num){
		
		int round = num;
		for( int i=1; i<=(num*2)-1 ; i++ ){
			
			for( int r=1; r<=num ; r++ ){
				
				if(r >= round){
					System.out.print("*");
				}else {
					System.out.print("_");
				}
				
			}

			System.out.println();
			
			if(i<num) {
				round--;
			}else{
				round++;
			}
		}
		
		System.out.println("");
		
	}
	
	public static void draw21(int num){
		
		int round = num;
		int count = 1;
		for( int i=1; i<=(num*2)-1 ; i++ ){
			
			for( int r=1; r<=num ; r++ ){
				
				if(r >= round){
					System.out.print(count++);
				}else {
					System.out.print("_");
				}
				
			}

			System.out.println();
			
			if(i<num) {
				round--;
			}else{
				round++;
			}
		}
		
		System.out.println("");
		
	}
	
	public static void draw22(int num){
		int left = num;
		int right = num;
		
		for( int i=1; i<=num ; i++ ){
			
			for( int r=1; r<=(num*2)-1 ; r++ ){

				if(r >= left && r <= right){
					System.out.print("*");
				}else {
					System.out.print("-");
				}
			}
			
			System.out.println("");
			left--;
			right++;

		}
		
		System.out.println("");
		
	}
	
	public static void draw23(int num){
		
		int left = 1;
		int right = (num*2)-1;
		
		for( int i=1; i<=num ; i++ ){
			
			for( int r=1; r<=(num*2)-1 ; r++ ){

				if(r >= left && r <= right){
					System.out.print("*");
				}else {
					System.out.print("-");
				}
			}
			
			System.out.println("");
			left++;
			right--;

		}
		
		System.out.println("");
		
	}
	
	public static void draw24(int num){
		
		int left = num;
		int right = num;
		
		for( int i=1; i<=(num*2)-1 ; i++ ){
			
			for( int r=1; r<=(num*2)-1 ; r++ ){

				if(r >= left && r <= right){
					System.out.print("*");
				}else {
					System.out.print("-");
				}
			}
			
			System.out.println("");
			if(i<num) {
				left--;
				right++;
			}else {
				left++;
				right--;
			}
			
		}
		
		System.out.println("");
		
	}
	
	public static void draw25(int num){
		
		int left = num;
		int right = num;
		int count = 1;
		for( int i=1; i<=(num*2)-1 ; i++ ){
			
			for( int r=1; r<=(num*2)-1 ; r++ ){

				if(r >= left && r <= right){
					System.out.print(count++);
				}else {
					System.out.print("-");
				}
			}
			
			System.out.println("");
			if(i<num) {
				left--;
				right++;
			}else {
				left++;
				right--;
			}
			
		}
		
		System.out.println("");
		
	}

}
