public class HW1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		draw1(2);
		draw1(3);
		draw1(4);
		draw1(5);
		
		System.out.println("");
		
		
		draw2(2);
		draw2(3);
		draw2(4);
		draw2(5);
		
		
		draw3(2);
		draw3(3);
		draw3(4);
		draw3(5);
		
		
		draw4(2);
		draw4(3);
		draw4(4);
		draw4(5);
		
		System.out.println("");
		
		draw5(2);
		draw5(3);
		draw5(4);
		draw5(5);
		
		draw6(2);
		draw6(3);
		draw6(4);
		draw6(5);
		
		draw7(2);
		draw7(3);
		draw7(4);
		draw7(5);
		
		draw8(2);
		draw8(3);
		draw8(4);
		draw8(5);
		
	}
	
	
	public static void draw1(int num){

		for( int i=0; i<num ; i++ ){
			System.out.print("*");
		}
		System.out.println("");
		
	}
	
	
	public static void draw2(int num){
		
		for( int i=0; i<num ; i++ ){
			
			for( int r=0; r<num ; r++ ){
				System.out.print("*");
			}
			System.out.println("");
			
		}
		System.out.println("");
	
	}
	
	
	public static void draw3(int num){
		
		for( int i=1; i<=num ; i++ ){
			for( int r=1; r<=num ; r++ ){
				System.out.print(r);
			}
			System.out.println("");
		}
		System.out.println("");

	}
	
	
	public static void draw4(int num){
		
		for( int i=num; i>=1 ; i-- ){
			
			for( int r=num; r>=1 ; r-- ){
				System.out.print(r);
			}
			
			System.out.println("");
		}
		System.out.println("");

	}
	
	
	public static void draw5(int num){
		
		for( int i=1; i<=num ; i++ ){
			for( int r=1; r<=num ; r++ ){
				System.out.print(i);
			}
			System.out.println("");
		}
		System.out.println("");

	}
	
	
	public static void draw6(int num){
		
		for( int i=num; i>=1 ; i-- ){
			
			for( int r=num; r>=1 ; r-- ){
				System.out.print(i);
			}
			
			System.out.println("");
		}
		System.out.println("");
		
		
	}
	
	
	public static void draw7(int num){
		
		int x = 1;
		for( int i=1; i<=num ; i++ ){
			for( int r=1; r<=num ; r++ ){
				System.out.print(x+" ");
				x++;
			}
			System.out.println("");
		}
		System.out.println("");
			
	}
	
	
	public static void draw8(int num){
		
		int x = num * num;
		for( int i=num; i>=1 ; i-- ){
			
			for( int r=num; r>=1 ; r-- ){
				System.out.print(x+" ");
				x--;
			}
			
			System.out.println("");
		}
		System.out.println("");
		
	}

}
