
public class HW2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[][] table = {
				{ "1", "2", "3" }, 
				{ "4", "5", "6" }, 
				{ "7", "8", "9" }
		};
		
		multiplyTable(table);
		
	}
	
	public static void multiplyTable(String[][] table){
		
		for (int row = 0; row < table.length; row++){
			for (int element = 0; element < table[row].length; element++){
				
				int newNum = Integer.parseInt( table[row][element] );
				
				table[row][element] = Integer.toString( newNum*2 ) ;
				
			}
		}
		
		for (int row = 0; row < table.length; row++){
			for (int element = 0; element < table[row].length; element++){
					System.out.print( table[row][element]+" " );
			}
			System.out.println("");
		}
		
	}

}
