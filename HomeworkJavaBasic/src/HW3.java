
public class HW3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		draw9(2);
		draw9(3);
		draw9(4);
		draw9(5);
	
		draw10(2);
		draw10(3);
		draw10(4);
		draw10(5);
		
		draw11(2);
		draw11(3);
		draw11(4);
		draw11(5);
		
		draw12(2);
		draw12(3);
		draw12(4);
		draw12(5);
		
		draw13(2);
		draw13(3);
		draw13(4);
		draw13(5);
		
		draw14(2);
		draw14(3);
		draw14(4);
		draw14(5);		
		
		draw15(2);
		draw15(3);
		draw15(4);
		draw15(5);
		
		draw16(2);
		draw16(3);
		draw16(4);
		draw16(5);
		
		draw17(2);
		draw17(3);
		draw17(4);
		draw17(5);
		
	}
	
	
	public static void draw9(int num){
		
		int x = 0;
		for( int i=0; i<num ; i++ ){
			System.out.println(x);
			x += 2;
		}
		System.out.println("");
		
	}
	
	public static void draw10(int num){
		
		int x = 2;
		for( int i=0; i<num ; i++ ){
			System.out.println(x);
			x += 2;
		}
		System.out.println("");
		
	}
	
	public static void draw11(int num){
		
		for( int i=1; i<=num ; i++ ){
			int x =i;
			for( int r=1; r<=num ; r++ ){

				System.out.print(x+" ");
				x+=i;
			}
			System.out.println();
		}
		
	}
	
	public static void draw12(int num){
		
		for( int i=0; i<num ; i++ ){
			for( int r=0; r<num ; r++ ){
				if(r == i){
					System.out.print("_");
				}else {
					System.out.print("*");
				}
			}
			System.out.println();
		}
		System.out.println("");
		
	}
	
	public static void draw13(int num){
		int round = num;
		
		for( int i=0; i<num ; i++ ){
			
			for( int r=0; r<num ; r++ ){
				
				if(r == round-1){
					System.out.print("_");
				}else {
					System.out.print("*");
				}
				
			}

			System.out.println();
			round--;
		}
		
		System.out.println("");
		
	}
	
	public static void draw14(int num){
		int round = 1;
		
		for( int i=0; i<num ; i++ ){
			
			for( int r=0; r<num ; r++ ){
				
				if(r <= round-1){
					System.out.print("*");
				}else {
					System.out.print("_");
				}
				
			}

			System.out.println();
			round++;
		}
		
		System.out.println("");
		
	}
	
	public static void draw15(int num){
		int round = num;
		for( int i=0; i<num ; i++ ){
			
			for( int r=0; r<num ; r++ ){
				
				if(r > round-1){
					System.out.print("_");
				}else {
					System.out.print("*");
				}
				
			}

			System.out.println();
			round--;
		}
		
		System.out.println("");
		
	}
	
	public static void draw16(int num){
		
		int round = 1;
		
		for( int i=1; i<=num+(num-1) ; i++ ){
			
			for( int r=1; r<=num ; r++ ){
				
				if(r <= round){
					System.out.print("*");
				}else {
					System.out.print("_");
				}
				
				
			}
			if(i<num){
				round++;
			}else {
				round--;
			}
			
			System.out.println("");
		}
		
		System.out.println("");
		
	}

	public static void draw17(int num){
		
		int round = 1;
		int count= 1;
		for( int i=1; i<=num+(num-1) ; i++ ){
			
			for( int r=1; r<=num ; r++ ){
				
				if(r <= round){
					System.out.print(count);
				}else {
					System.out.print("_");
				}
				
				
			}
			if(i<num){
				count++;
				round++;
			}else {
				count--;
				round--;
			}
			
			System.out.println("");
		}
		
		System.out.println("");
		
	}

}
	
	
	
	

